# Mining your logfiles

## Installing Docker

The examples run using docker and docker-compose, please ensure you have docker and docker-compose setup and running on your machine.

To setup docker in linux use the following link:
https://docs.docker.com/install/linux/docker-ce/ubuntu/#set-up-the-repository

For Windows use the following link:
https://docs.docker.com/docker-for-windows/install/

To install docker-compose use the following link:
https://docs.docker.com/compose/install/#install-compose

## Pull docker images

For this examples shown at DevConf 2018, pull the images as follows:

Open source ELK stack

* docker pull docker.elastic.co/elasticsearch/elasticsearch-oss:6.2.2
* docker pull docker.elastic.co/logstash/logstash-oss:6.2.2
* docker pull docker.elastic.co/kibana/kibana-oss:6.2.2

Free license ELK stack

* docker pull docker.elastic.co/elasticsearch/elasticsearch:6.2.2
* docker pull docker.elastic.co/logstash/logstash:6.2.2
* docker pull docker.elastic.co/kibana/kibana:6.2.2

Grafana

* docker pull grafana/grafana:5.0.0-beta5

## Install filebeat

Install filebeat using the follwoign link:
https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-installation.html

## Initiaise environment

If on linux run the bash script ./initialise_env.sh to set the variable vm.max_map_count to the correct value

## Running demo 1

Start the ELK stack by running the bash script ./start_elk_oss.sh

Run the first demo import using the command ./run_demo_1.sh

You can clean up demo 1 by running the following ./clean_oss.sh

## Running demo 2

Start the ELK stack by running the bash script ./start_elk_iss_logging.sh

Run the first demo import using the command ./run_demo_2.sh

You can clean up demo 2 by running the following ./clean_elk_iss.sh

## Grafana demo

When runnign the grafana demo ensure that demo 2 is running and has imported data.

To start the grafana demo run the bash script ./start_grafana.sh

