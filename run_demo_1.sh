#!/bin/bash

echo "cleaning up test data directory ..."

find ${PWD}/testData/ -type f -iname \*.log -delete

echo "reset filebeat registry"

sudo rm /var/lib/filebeat/registry

echo "copy new import data ..."

cp ${PWD}/sampleData/sample/*.log ${PWD}/testData/

echo "running filebeat ..."

sudo /usr/bin/filebeat -e -c ${PWD}/beats/filebeats/apachelogs/apache.yml -d "publish"


