#!/bin/bash

docker rm elk_elasticsearchoss_1
docker rm elk_kibanaoss_1
docker rm elk_logstashoss_1

docker volume rm elk_es_oss_data