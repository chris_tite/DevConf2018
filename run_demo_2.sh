#!/bin/bash

echo "cleaning up test data directory ..."

find ${PWD}/testData/ -type f -iname \*.log -delete

echo "reset filebeat registry"

sudo rm /var/lib/filebeat/registry

echo "copy new import data ..."

cp ${PWD}/sampleData/ecr/*.log ${PWD}/testData/

echo "running filebeat ..."

sudo /usr/bin/filebeat -e -c ~/ops/devops/elk/beats/filebeats/iislogs/iis.yml -d "publish"
